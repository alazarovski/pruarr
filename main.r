library(sf)
library(mapview)
library(units)
library(sf)

CPF_data <- CPF_data %>% filter(!(is.na(LON) | is.na(LAT) | is.na(SAM_ID)))

# the utm_prj4 calculates the UTM zone for each of the airports. This is done to adjust the TMA radius
# according to the geographical location of the airports.
utm_prj4 <- function(x) {
  coords <-
    cbind(x, st_coordinates(x)) # get x and y coordinates from geometry column
  long <- coords$X # make vector from x column
  lat <- coords$Y # make vector from y column
  zone <-
    if (lat >= 56 &&
        lat < 64 && long >= 3 && long < 12) {
      x <- 32
    } else if (lat >= 72 &&
               lat < 84 && long >= 0 && long < 9) {
      x <- 31
    } else if (lat >= 72 &&
               lat < 84 && long >= 9 && long < 21) {
      x <- 33
    } else if (lat >= 72 &&
               lat < 84 && long >= 21 && long < 33) {
      x <- 35
    } else if (lat >= 72 &&
               lat < 84 && long >= 33 && long < 42) {
      x <- 37
    } else{
      x <- (floor((long + 180) / 6) %% 60) + 1
    }
  
  prj <- purrr::map2_chr(zone, lat, function(y, z) {
    if (z >= 0) {
      paste0("+proj=utm +zone=",
             y,
             " +datum=WGS84 +units=m +no_defs")
    } else{
      paste0("+proj=utm +zone=",
             y,
             " +south",
             " +datum=WGS84 +units=m +no_defs")
    }
  })
  prj
}

# reading one day data
read_cpf <- function(CPF_data) {
  CPF_data %>% mutate(TIME_OVER = as.numeric(TIME_OVER)) %>% group_by(SAM_ID) %>% arrange(TIME_OVER) %>% 
    select(SAM_ID, TIME_OVER, LON, LAT) %>%
    st_as_sf(coords = c("LON", "LAT"), dim = "XY") %>% st_set_crs(4326) %>%
    group_by(SAM_ID) %>% summarise(geometry = st_union(geometry), do_union = FALSE) %>% st_cast("LINESTRING")
}
df <- read_cpf(CPF_data)


make_areas <- function(pol,rad) {
  # manually create airport coordinates
  pol <- data.frame(iata_code = "CDG", lon = 2.5477778, lat = 49.0097222) %>% 
    st_as_sf(coords = c("lon", "lat"), dim = "XY") %>%
    st_set_crs(4326)
  
  # pru_tma is used for assigning a value for the TMA exclusion radius that will be used in the pruhfe
  pru_tma <- function(radius) {
    set_units(radius, as_units("nmile"), mode = "standard") %>%
      set_units(as_units("m"), mode = "standard")
  }
  r <- pru_tma(rad)
  
  # the pru_polygon function calculates circle polygons around every airport either ADES or ADEP taking
  # into account the UTM zone
  pru_polygon <- function(pol, r) {
    map2(1:length(pol$geometry), utm_prj4(pol), function(x, y) {
      st_transform(pol[x,], y)
    }) %>%
      map(~ st_buffer(., r)) %>%                 # make buffer with previously assigned radius -r
      map(~ st_transform(., 4326)) %>%           # make sure all sf objects have same crs
      reduce(rbind)
  }
  pol1 <- pru_polygon(pol, r)
}
t1 <- make_areas(pol, 8)
t2 <- make_areas(pol, 40)
t3 <- make_areas(pol, 100)
t4 <- make_areas(pol, 200) 

tr1 <- st_intersection(df, t1) %>% mutate(r = 8)
tr2 <- st_intersection(df, t2) %>% mutate(r = 40)
tr3 <- st_intersection(df, t3) %>% mutate(r = 100)
tr4 <- st_intersection(df, t4) %>% mutate(r = 200)

tr <- rbind(tr1, tr2, tr3, tr4)

tr <- tr %>% mutate(trj_len = as.numeric(st_length(geometry))/1852)

date <- CPF_data %>% group_by(SAM_ID, VERSION_DATE) %>% filter(row_number() == 1)

fdf <- left_join(tr, date, by = "SAM_ID") %>% st_set_crs(4326)

st_write(fdf, "CPF 16-28 2017.shp")

df <- sf::st_read("CPF 31-15 2017.shp")






read_cpf_t <- function(CPF_data) {
  CPF_data %>% mutate(TIME_OVER = as.numeric(TIME_OVER)) %>% group_by(SAM_ID) %>% arrange(TIME_OVER) %>% 
    select(SAM_ID, TIME_OVER, LON, LAT) %>%
    st_as_sf(coords = c("LON", "LAT"), dim = "XY") %>% st_set_crs(4326) 
}
df <- read_cpf_t(CPF_data)
df1 <- df[1,] %>% st_as_sf() %>% st_set_crs(4326)

# The trajectory in format "linestring" needs to be split according to intersections with TMA polygons
pru_ls2mls <- function(df, pol_mls) {
  difference <- function(ls, pl) {
    st_difference(ls, st_buffer(st_intersection(ls, pl), dist = 1e-12))
  }
  prepare_mls <- function(mls, df) {
    nogeom_df <-
      df[, c(1:3)]                                                       # make ls without geometry and put multilinestring from step before inside
    mls <- bind_cols(nogeom_df, mls) %>% st_as_sf() %>% st_set_crs(4326)
    
  }
  st_sf(geometry = st_sfc(purrr::map2(df$geometry, pol_mls$geometry, difference))) %>%
    st_set_crs(4326) %>%
    prepare_mls(df)
}
points <- pru_ls2mls(df, pol)

inters <- inters %>% mutate(L = as.numeric(st_length(geometry))/1852) %>% 
  group_by(SAM_ID) %>% 
  mutate(L = as.numeric(st_length(geometry))/1852,
         fp = st_cast(geometry, "POINT")[1],# first point
         lp = st_cast(geometry, "POINT")[length(st_cast(geometry, "POINT"))],
         gc = st_distance(fp,lp) / 1852,
         extns = ((L-gc)/gc) * 100) # last point
  
ggplot(inters) + geom_histogram(aes(extns))

inters %>% filter(L > 83) %>% mapview()

